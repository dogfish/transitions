Custom Transitions and Animations
=========================================

A UIKit custom modal transition that simulates an elastic drag and different transitions and button animation in Swift.

Animations are eye catchy,neither the presenter not the audience can ignore it.One can grab users attention through animations.

Visual effects are more easy to recollect and follow.

## Features of the app   

* Push & Pop transition
* Present & Dismiss transition 
* Supports Swing and Circular transition
* Zoom_In and Zoom_Out Transition
* Button Animations
* Elastic Drag



## Requirements   

* iOS 8.0 or later
* Xcode 8 or higher
* Swift 3.0



## Installation

### CocoaPods    

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command in terminal:

```bash
$ gem install cocoapods
```
You need to integrate MotionAnimation and Canvas CocoaPods into you Xcode project for Button Animations and Elastic Drag


* Go to Terminal --->Goto your project folder 

```bash
$ pod init
```
* After this open you project folder in finder you will see Podfile

* To integrate MotionAnimation and Canvas into your Xcode project using CocoaPods, specify the following pod command in your `Podfile`:

```bash
use_frameworks!

pod 'MotionAnimation'

pod 'Canvas'
```



Then, run the following command from terminal:

```bash
$ pod install
```


In Button Animation viewController, don't forget to import Canvas and 
import MotionAnimation for Elastic Drag.



To build the module, you need to clone git repository and build the sources :

    # clone the repository
    git clone https://bitbucket.org/dogfish/transitions

As you have used CocoaPods now you need to open .xcworkspace project and not .xcodeproj project.


### How does it work? ###


##  Checkout Example folder for individual transitions and its implementation##
--------------

* For the below example checkout Example/SwingTransition

* You need to include WYInteractiveTransitions.swift file in your project

* Do required changes in ViewController and don't forget to add showSegue     
  as identifier in StoryBoard Segue under Attribute Inspector

![](https://i.imgflip.com/1kk1uy.gif)
---------
--------
* For the below example checkout Example/ButtonAnimations
* You need to include cocoa pod 'Canvas' and import it in your controller.
* Here the button should be placed in the view and in the identity   
  inspector set the class of the button as CSAnimationView.
* Set the required user defined runtime attributes as required.

![](https://i.imgflip.com/1kk219.gif)
-----------

--------
* For the below example include Zoom_In_Out.swift file in your project and do necessary changes in viewController.swift file.
* Include required class for connector in attribute inspector.
 class Zoom_In_Out for zoom in and class UnWindScaleSegue for zoom out 

![](https://i.imgflip.com/1kk2zx.gif)


--------------

* For the below example checkout Example/CircularTransition

* You need to include CircularTransition.swift file in your project

* Do required changes in ViewController and don't forget to override prepare function and include method animationController for present and dismiss

![](https://i.imgflip.com/1kk365.gif)
---------------

* Same steps as Present & Dismiss

![](https://i.imgflip.com/1kk3hw.gif)

![](https://i.imgflip.com/1kk3mr.gif)

---------------


* For the below example checkout Example/testElasticity
* You need to include cocoa pod 'MotionAnimation' and import it in your controller.

![](https://i.imgflip.com/1kk3pl.gif)

## Author

* **Nafisa Hasan**
--------------



### References ###

* http://mathewsanders.com/animated-transitions-in-swift/

* https://www.youtube.com/watch?v=B9sH_VxPPo4&t=513s

* https://developer.apple.com/reference/uikit/uiviewcontroller

* https://github.com/lkzhao/ElasticTransition/blob/master/README.md